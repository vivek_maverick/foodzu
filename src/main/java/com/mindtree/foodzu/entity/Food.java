package com.mindtree.foodzu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="food")
public class Food {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="food_id")
	private int foodId;
	
	@Column(name="food_name")
	private String foodName;
	
	@Column(name="food_type")
	private int foodType;
	
	@Column(name="food_price")
	private int foodPrice;
	
	@Column(name="food_rating")
	private int foodRating;

	@Column(name="food_frequency")
	private int foodFrequency;

	public Food() {
		super();
	}

	public Food(int foodId, String foodName, int foodType, int foodPrice, int foodRating, int foodFrequency) {
		super();
		this.foodId = foodId;
		this.foodName = foodName;
		this.foodType = foodType;
		this.foodPrice = foodPrice;
		this.foodRating = foodRating;
		this.foodFrequency = foodFrequency;
	}

	public int getFoodId() {
		return foodId;
	}

	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public int getFoodType() {
		return foodType;
	}

	public void setFoodType(int foodType) {
		this.foodType = foodType;
	}

	public int getFoodPrice() {
		return foodPrice;
	}

	public void setFoodPrice(int foodPrice) {
		this.foodPrice = foodPrice;
	}

	public int getFoodRating() {
		return foodRating;
	}

	public void setFoodRating(int foodRating) {
		this.foodRating = foodRating;
	}

	public int getFoodFrequency() {
		return foodFrequency;
	}

	public void setFoodFrequency(int foodFrequency) {
		this.foodFrequency = foodFrequency;
	}

	@Override
	public String toString() {
		return "Food [foodId=" + foodId + ", foodName=" + foodName + ", foodType=" + foodType + ", foodPrice="
				+ foodPrice + ", foodRating=" + foodRating + ", foodFrequency=" + foodFrequency + "]";
	}

}
