package com.mindtree.foodzu.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="restaurant")
public class Restaurant {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="restaurant_id")
	private int restaurantId;
	
	@Column(name="restaurant_name")
	private String restaurantName;
	
	@Column(name="restaurant_rating")
	private int restaurantRating;
	
	@Column(name="restaurant_image",length=100000)
	private String restaurantImage;
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="restaurant_id")
	List<Food> menu;

	public Restaurant() {
		super();
	}

	public Restaurant(int restaurantId, String restaurantName, int restaurantRating, String restaurantImage,
			List<Food> menu) {
		super();
		this.restaurantId = restaurantId;
		this.restaurantName = restaurantName;
		this.restaurantRating = restaurantRating;
		this.restaurantImage = restaurantImage;
		this.menu = menu;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getRestaurantName() {
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}
	
	

	public String getRestaurantImage() {
		return restaurantImage;
	}

	public void setRestaurantImage(String restaurantImage) {
		this.restaurantImage = restaurantImage;
	}

	public int getRestaurantRating() {
		return restaurantRating;
	}

	public void setRestaurantRating(int restaurantRating) {
		this.restaurantRating = restaurantRating;
	}

	

	public List<Food> getMenu() {
		return menu;
	}

	public void setMenu(List<Food> menu) {
		this.menu = menu;
	}

	@Override
	public String toString() {
		return "Restaurant [restaurantId=" + restaurantId + ", restaurantName=" + restaurantName + ", restaurantRating="
				+ restaurantRating + ", restaurantImage=" + restaurantImage + ", menu=" + menu + "]";
	}	
	
	
}
