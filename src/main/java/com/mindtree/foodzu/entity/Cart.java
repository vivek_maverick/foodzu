package com.mindtree.foodzu.entity;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cart")
public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cart_id")
	private int cartId;

	@OneToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "restaurant_id")
	private Restaurant restaurant;
/*	
//	@OneToMany
//	@CollectionTable(name = "food")
//	@MapKeyColumn(name = "cart_id")
//	@Column(name = "food_id")
//	private Map<Food, Integer> foodList = new HashMap<Food, Integer>();
*/
	@ElementCollection
	@CollectionTable(joinColumns = @JoinColumn(name = "cart_joined"))
	@Column(name = "cart_food_quantity")
	@MapKeyJoinColumn(name = "food_id")
	private Map<Food, Integer> foodList = new HashMap<Food, Integer>();

	public Cart() {
		super();
	}

	public Cart(int cartId, Customer customer, Restaurant restaurant, Map<Food, Integer> foodList) {
		super();
		this.cartId = cartId;
		this.customer = customer;
		this.restaurant = restaurant;
		this.foodList = foodList;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Map<Food, Integer> getFoodList() {
		return foodList;
	}

	public void setFoodList(Map<Food, Integer> foodList) {
		this.foodList = foodList;
	}

	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", customer=" + customer + ", restaurant=" + restaurant + ", foodList="
				+ foodList + "]";
	}
	
}
