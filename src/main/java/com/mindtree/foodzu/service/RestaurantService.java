package com.mindtree.foodzu.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.mindtree.foodzu.dto.FoodDTO;
import com.mindtree.foodzu.entity.Restaurant;

public interface RestaurantService {

	Restaurant addRestaurant(Restaurant restaurant) throws FileNotFoundException, IOException;

	String deleteRestaurant(int restaurantId);

	String addFoodToRestaurant(List<FoodDTO> foodDtoList);

	List<Restaurant> getAllRestaurant();

}
