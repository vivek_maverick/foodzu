package com.mindtree.foodzu.service;

import java.util.List;

import com.mindtree.foodzu.dto.FoodDTO;

public interface FoodService {

	public String deleteFood(int foodId);

	public String addFood(List<FoodDTO> foodDtoList);
	
	public String updateFood(FoodDTO foodDto);
	
}
