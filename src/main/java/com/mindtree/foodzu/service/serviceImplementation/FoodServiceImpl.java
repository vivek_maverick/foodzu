package com.mindtree.foodzu.service.serviceImplementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.foodzu.dto.FoodDTO;
import com.mindtree.foodzu.entity.Food;
import com.mindtree.foodzu.repository.FoodRepository;
import com.mindtree.foodzu.service.FoodService;
import com.mindtree.foodzu.service.RestaurantService;

@Service
public class FoodServiceImpl implements FoodService {
	
	@Autowired
	FoodRepository foodRepository;
	
	@Autowired 
	RestaurantService restaurantService; 
	
	@Override
	public String deleteFood(int foodId) {
		foodRepository.deleteById(foodId);
		return "Deleted";
	}

	@Override
	public String addFood(List<FoodDTO> foodDtoList) {
		return restaurantService.addFoodToRestaurant(foodDtoList);
	}

	@Override
	public String updateFood(FoodDTO foodDto) {
		
		Food food = foodRepository.findById(foodDto.getFoodId()).get();
		
		food.setFoodFrequency(foodDto.getFoodFrequency());
		food.setFoodName(foodDto.getFoodName());
		food.setFoodPrice(foodDto.getFoodPrice());
		food.setFoodRating(foodDto.getFoodRating());
		food.setFoodType(foodDto.getFoodType());
		
		foodRepository.save(food);
		
		return "Updated";
		
		
		
	}

}
