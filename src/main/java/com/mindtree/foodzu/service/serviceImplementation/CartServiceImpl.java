package com.mindtree.foodzu.service.serviceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.foodzu.entity.Cart;
import com.mindtree.foodzu.repository.CartRepository;
import com.mindtree.foodzu.service.CartService;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	CartRepository cartRepository;
	
	@Override
	public Cart getcart(Cart cart) {
		Cart obj = cartRepository.findById(cart.getCartId()).get();
		System.out.println(obj);
		return null;
	}

}
