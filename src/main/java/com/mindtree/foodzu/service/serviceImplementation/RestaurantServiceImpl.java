package com.mindtree.foodzu.service.serviceImplementation;

import java.io.File;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.foodzu.dto.FoodDTO;
import com.mindtree.foodzu.entity.Food;
import com.mindtree.foodzu.entity.Restaurant;
import com.mindtree.foodzu.repository.RestaurantRepository;
import com.mindtree.foodzu.service.RestaurantService;

@Service
public class RestaurantServiceImpl implements RestaurantService {

	@Autowired
	RestaurantRepository restaurantRepository;

	@Override
	public Restaurant addRestaurant(Restaurant restaurant) throws IOException {
		
		
		
		ByteArrayOutputStream baos=new ByteArrayOutputStream(1000);
		BufferedImage img=ImageIO.read(new File("C://Screenshot.png"));
		ImageIO.write(img, "png", baos);
		baos.flush();
		String base64String=Base64.encode(baos.toByteArray());
		restaurant.setRestaurantImage(base64String);
		System.out.println(base64String);
		baos.close();
		
		
		
		
//		File image = new File("C://Screenshot.png");
//		InputStream inputStream = new FileInputStream(image);
//		restaurant.setRestaurantImage(IOUtils.toByteArray(inputStream));
		return restaurantRepository.save(restaurant);
	}

	@Override
	public String deleteRestaurant(int restaurantId) {
		restaurantRepository.deleteById(restaurantId);
		return "Successfully Deleted";
	}

	@Override
	public String addFoodToRestaurant(List<FoodDTO> foodDtoList) {
		Restaurant restaurantToBeUpdated = restaurantRepository.getOne(foodDtoList.get(0).getRestaurantId());

		List<Food> new_menu = restaurantToBeUpdated.getMenu();

		foodDtoList.forEach(foodItem -> {
			Food food = new Food();
			food.setFoodName(foodItem.getFoodName());
			food.setFoodPrice(foodItem.getFoodPrice());
			food.setFoodFrequency(foodItem.getFoodFrequency());
			food.setFoodRating(foodItem.getFoodRating());
			food.setFoodType(foodItem.getFoodType());

			new_menu.add(food);
		});

		restaurantToBeUpdated.setMenu(new_menu);

		restaurantRepository.save(restaurantToBeUpdated);
		System.out.println("After Restaurant is updated");
		return "Food successfully added";
	}

	@Override
	public List<Restaurant> getAllRestaurant() {
		return restaurantRepository.findAll();
	}
}
