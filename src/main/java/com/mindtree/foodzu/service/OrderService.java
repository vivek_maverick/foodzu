package com.mindtree.foodzu.service;

import com.mindtree.foodzu.dto.OrderDTO;

public interface OrderService {

	public String addOrder(OrderDTO order);
	
}
