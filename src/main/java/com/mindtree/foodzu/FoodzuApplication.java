package com.mindtree.foodzu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodzuApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodzuApplication.class, args);
	}

}