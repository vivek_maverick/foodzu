package com.mindtree.foodzu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.foodzu.dto.FoodDTO;
import com.mindtree.foodzu.service.FoodService;

@CrossOrigin
@RestController
@RequestMapping("/food")
public class FoodController {
	
	@Autowired
	FoodService foodService;
	
	@DeleteMapping("/")
	public String deleteFood(@RequestBody int foodId) {
		return foodService.deleteFood(foodId);
	}

	@PostMapping("/")
	public String addFood(@RequestBody List<FoodDTO> foodDtoList) {
		return foodService.addFood(foodDtoList);
	}
	
	@PutMapping("/")
	public String updateFood(@RequestBody FoodDTO foodDto) {
		return foodService.updateFood(foodDto);
	}
}
