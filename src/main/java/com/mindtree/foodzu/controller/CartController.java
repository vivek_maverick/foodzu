package com.mindtree.foodzu.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.foodzu.entity.Cart;
import com.mindtree.foodzu.service.CartService;

@CrossOrigin
@RestController
@RequestMapping("/cart")
public class CartController {
	
	@Autowired
	CartService cartService;
	
	@PostMapping("/")
	public ResponseEntity<?> saveCart(@RequestBody Cart cart){
		//try {
			Map<String, Object> result = new HashMap<>();
			//result.put("body",empService.addEmployee(emp));
			result.put("body", cartService.getcart(cart));
			result.put("status", HttpStatus.OK);
			result.put("success", true);
			return ResponseEntity.status(HttpStatus.OK).header("message", String.valueOf(HttpStatus.OK) + "_done")
					.body(result);
//		} catch (ApplicationException exception) {
//			Map<String, Object> result = new HashMap<>();
//			result.put("status", HttpStatus.NOT_FOUND);
//			result.put("success", false);
//			result.put("exception", exception.getMessage());
//			return ResponseEntity.status(HttpStatus.OK).header("status", String.valueOf(HttpStatus.NOT_FOUND))
//					.body(result);
//		}
//		 catch (Exception e) {
//				Map<String, Object> result = new HashMap<String, Object>();
//				result.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
//				result.put("success", false);
//				result.put("message", "Error while fetching Employee Details");
//				result.put("exception", e.getMessage());
//				result.put("cause", e.getCause());
//				return ResponseEntity.status(HttpStatus.OK)
//						.header("status", String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR)).body(result);
//			}
	}
	
	//@PostMapping()

}
