package com.mindtree.foodzu.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.foodzu.entity.Restaurant;
import com.mindtree.foodzu.service.RestaurantService;



@CrossOrigin
@RestController
@RequestMapping("/restaurant")
public class RestaurantController {
	
	@Autowired
	RestaurantService restaurantService;
	
	@GetMapping("/")
	public String getAllRestaurants(){
		System.out.println(new String(restaurantService.getAllRestaurant().get(0).getRestaurantImage()));
//		return restaurantService.getAllRestaurant();
		return restaurantService.getAllRestaurant().get(0).getRestaurantImage();
	}
	@PostMapping("/")
	public Restaurant addRestaurant(@RequestBody Restaurant restaurant) throws FileNotFoundException, IOException{
		return restaurantService.addRestaurant(restaurant);
	}
	@DeleteMapping("/")
	public String deleteRestaurant(@RequestBody int restaurantId){
		return restaurantService.deleteRestaurant(restaurantId);
	}
	

}
