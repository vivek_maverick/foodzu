package com.mindtree.foodzu.dto;

public class FoodDTO {
	
	private int foodId;
	private String foodName;
	private int foodType;
	private int foodPrice;
	private int foodRating;
	private int foodFrequency;
	private int restaurantId;
	
	public FoodDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FoodDTO(int foodId, String foodName, int foodType, int foodPrice, int foodRating, int foodFrequency,
			int restaurantId) {
		super();
		this.foodId = foodId;
		this.foodName = foodName;
		this.foodType = foodType;
		this.foodPrice = foodPrice;
		this.foodRating = foodRating;
		this.foodFrequency = foodFrequency;
		this.restaurantId = restaurantId;
	}

	public int getFoodId() {
		return foodId;
	}

	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public int getFoodType() {
		return foodType;
	}

	public void setFoodType(int foodType) {
		this.foodType = foodType;
	}

	public int getFoodPrice() {
		return foodPrice;
	}

	public void setFoodPrice(int foodPrice) {
		this.foodPrice = foodPrice;
	}

	public int getFoodRating() {
		return foodRating;
	}

	public void setFoodRating(int foodRating) {
		this.foodRating = foodRating;
	}

	public int getFoodFrequency() {
		return foodFrequency;
	}

	public void setFoodFrequency(int foodFrequency) {
		this.foodFrequency = foodFrequency;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

}
