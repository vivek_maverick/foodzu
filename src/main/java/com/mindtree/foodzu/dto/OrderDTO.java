package com.mindtree.foodzu.dto;

import java.sql.Date;
import java.util.List;

import com.mindtree.foodzu.entity.Food;

public class OrderDTO {
	
	private Date orderDate;
	
	private int restaurantId;
	
	private int customerId;
	
	private List<Food> food;
	
	public OrderDTO() {
		super();
	}

	public OrderDTO(Date orderDate, int restaurantId, int customerId, List<Food> food) {
		super();
		this.orderDate = orderDate;
		this.restaurantId = restaurantId;
		this.customerId = customerId;
		this.food = food;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public List<Food> getFood() {
		return food;
	}

	public void setFood(List<Food> food) {
		this.food = food;
	}

	@Override
	public String toString() {
		return "OrderDTO [orderDate=" + orderDate + ", restaurantId=" + restaurantId + ", customerId=" + customerId
				+ ", food=" + food + "]";
	}

}
