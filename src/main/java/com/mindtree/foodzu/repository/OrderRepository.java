package com.mindtree.foodzu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.foodzu.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> 
{

}
