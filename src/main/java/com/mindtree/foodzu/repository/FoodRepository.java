package com.mindtree.foodzu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.foodzu.entity.Food;

@Repository
public interface FoodRepository extends JpaRepository<Food, Integer> {

}
